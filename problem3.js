// ==== Problem #3 ====

// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const problem3 = inventory => {
  if (!Array.isArray(inventory) || !inventory) return null;
  if (inventory.length === 0) return null;
  let carModels = null;
  for (let i = 0; i < inventory.length; i++) {
    const car = inventory[i];
    carModels.push(car.car_model.toUpperCase());
  }
  return carModels.sort();
};

module.exports = problem3;
