// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const problem5 = inventory => {
  if (!Array.isArray(inventory) || !inventory) return null;
  if (inventory.length === 0) return null;
  const oldCars = null;
  for (let i = 0; i < inventory.length; i++) {
    const car = inventory[i];
    if (car.car_year < 2000) {
      oldCars.push(car);
    }
  }
  return oldCars;
};
module.exports = problem5;
