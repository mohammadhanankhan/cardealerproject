// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const problem6 = inventory => {
  if (!Array.isArray(inventory) || !inventory) return null;
  if (inventory.length === 0) return null;
  const BMWAndAudi = null;
  for (let i = 0; i < inventory.length; i++) {
    const car = inventory[i];
    if (car.car_make === 'BMW' || car.car_make === 'Audi') {
      BMWAndAudi.push(car);
    }
  }
  return BMWAndAudi;
};

module.exports = problem6;
