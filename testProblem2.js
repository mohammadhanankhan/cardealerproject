const problem2 = require('./problem2.js');
const inventory = require('./inventory.js');

const result = problem2(inventory);
if (result) {
  const { car_make, car_model } = result;
  console.log(`Last car is a ${car_make} ${car_model}`);
}
