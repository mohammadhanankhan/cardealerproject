const problem1 = require('./problem1.js');
const inventory = require('./inventory.js');

const result = problem1(inventory, 2345);
if (result) {
  const { id, car_year, car_make, car_model } = result;
  console.log(`Car ${id} is a ${car_year} ${car_make} ${car_model}`);
} else {
  return [];
}
